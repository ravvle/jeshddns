# Vultr Dynamic DNS

Supports A (ivp4) and AAAA (ipv6) records being updated for a single subdomain. Made in go so you can have binaries, but they're massive.

Run this with a cronjob at the moment: ```* * * * * jeshddns```

TODO: turn into a service as well as cron-able
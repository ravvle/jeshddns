package main

import "fmt"
import "net/http"
import "net/url"
import "io/ioutil"
import "encoding/json"
import "strconv"
import "strings"

var vultrapikey string
var vultrdomain string
var vultrsubdomain string

func dnsToString(d DNSRecord) string {
	return d.Type + " " + d.Name + " " + d.Data + " " + strconv.Itoa(d.Priority) + " " + strconv.Itoa(d.RECORDID) + " " + strconv.Itoa(d.TTL)
}

type IPS struct {
	v4 string
	v6 string
}

type Config struct {
	Apikey    string
	Domain    string
	Subdomain string
}

type DNSRecord struct {
	Type     string
	Name     string
	Data     string
	Priority int
	RECORDID int
	TTL      int
}

func main() {
	loadConfig()

	updateDNSList()
}

func loadConfig() {

	// read file
	data, err := ioutil.ReadFile("config.json")
	if err != nil {
		fmt.Print(err)
	}

	// json data
	var configitems Config

	// unmarshall it
	err = json.Unmarshal(data, &configitems)
	if err != nil {
		fmt.Println("error:", err)
	}

	vultrapikey = configitems.Apikey
	vultrdomain = configitems.Domain
	vultrsubdomain = configitems.Subdomain
}

func updateDNSList() {
	currentIPS := getCurrentIPS()

	var dnsitems []DNSRecord

	json.Unmarshal([]byte(getDnsList()), &dnsitems)

	for _, value := range dnsitems {
		if value.Name == vultrsubdomain {

			if value.Type == "AAAA" {
				if value.Data != currentIPS.v6 && currentIPS.v6 != "" {
					fmt.Println("updating!!!", value.Data, currentIPS.v6)
					if updateValue(value.RECORDID, currentIPS.v6) {
						fmt.Println("it worked!! (AAAA)")
					} else {
						fmt.Println("it didn't work!! (AAAA)")
					}
				} else {
					fmt.Println("didn't update old:",value.Data,"new:",currentIPS.v6)
				}
			} else if value.Type == "A" {
				if value.Data != currentIPS.v4 && currentIPS.v4 != "" {
					fmt.Println("updating!!!", value.Data, currentIPS.v4)
					if updateValue(value.RECORDID, currentIPS.v4) {
						fmt.Println("it worked!! (A)")
					} else {
						fmt.Println("it didn't work!! (A)")
					}
				} else {
					fmt.Println("didn't update old:",value.Data,"new:",currentIPS.v4)
				}
			}

			//fmt.Println(dnsToString(value))
		}
	}
}

func getDnsList() string {
	url := "https://api.vultr.com/v1/dns/records?domain=" + vultrdomain

	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("API-Key", vultrapikey)
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Host", "api.vultr.com")
	req.Header.Add("cache-control", "no-cache")

	res, conerr := http.DefaultClient.Do(req)

	if conerr == nil {
		defer res.Body.Close()
		body, readerr := ioutil.ReadAll(res.Body)

		if readerr == nil {
			if res.StatusCode == 200  {
				return string(body)
			} else{
				fmt.Println("api didn't like your request:", string(body))
			}
		}
	}

	fmt.Println("Failed to get the existing DNS record(s)")
	return ""
}

func updateValue(recordid int, newip string) bool {
	urll := "https://api.vultr.com/v1/dns/update_record"

	payload := url.Values{}

	payload.Set("RECORDID", strconv.Itoa(recordid))
	payload.Set("data", newip)
	payload.Set("domain", vultrdomain)
	payloadData := *strings.NewReader(payload.Encode())

	req, _ := http.NewRequest("POST", urll, &payloadData)

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("API-Key", vultrapikey)
	req.Header.Add("cache-control", "no-cache")

	res, err := http.DefaultClient.Do(req)

	if err == nil {
		defer res.Body.Close()
		//body, _ := ioutil.ReadAll(res.Body)

		if res.StatusCode == 200 {
			return true
		}
	} else {
		fmt.Println("Failed to update the DNS record(s)")
	}

	return false
}

func getCurrentIPS() IPS {

	url := "https://ipv4.icanhazip.com/"
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Add("Accept", "*/*")
	req.Header.Add("Host", "ipv4.icanhazip.com")
	req.Header.Add("cache-control", "no-cache")

	res, conerr := http.DefaultClient.Do(req)
	v4 := ""

	if conerr == nil {
		defer res.Body.Close()
		body, readerr := ioutil.ReadAll(res.Body)

		if readerr == nil {
			v4 = string(body)
		}
	} else {
		fmt.Println("ipv4 lookup failed", conerr)
	}

	url2 := "https://ipv6.icanhazip.com/"
	req2, _ := http.NewRequest("GET", url2, nil)
	req2.Header.Add("Accept", "*/*")
	req2.Header.Add("Host", "ipv6.icanhazip.com")
	req2.Header.Add("cache-control", "no-cache")

	res2, connerr2 := http.DefaultClient.Do(req2)
	v6 := ""

	if connerr2 == nil {
		defer res2.Body.Close()
		body2, readerr2 := ioutil.ReadAll(res2.Body)

		if readerr2 == nil {
			v6 = string(body2)
		}
	} else {
		fmt.Println("ipv6 lookup failed", connerr2)
	}

	return IPS{v4, v6}
}
